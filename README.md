# pe-tools

- [Wikipedia: Portable Executable](https://en.wikipedia.org/wiki/Portable_Executable)
- [Wikipedia: PE File Structure](https://en.wikipedia.org/wiki/Portable_Executable#/media/File:Portable_Executable_32_bit_Structure_in_SVG_fixed.svg)
- [A dive into the PE file format](https://0xrick.github.io/win-internals/pe8/)
- [How to read a struct from a file in rust](https://stackoverflow.com/questions/25410028/how-to-read-a-struct-from-a-file-in-rust)
- [.NET Assembly File Format](https://docs.microsoft.com/en-us/dotnet/standard/assembly/file-format)
- [ECMA-335 - Common Language Infrastructure (CLI)](https://www.ecma-international.org/publications-and-standards/standards/ecma-335/)
