# OMF

The MS-DOS OBJ file format is called Relocatable Object Module Format
(see [Relocatable Object Module Format](https://en.wikipedia.org/wiki/Relocatable_Object_Module_Format)).
It is standardized in [OMF_v1.1.pdf](https://pierrelib.pagesperso-orange.fr/exec_formats/OMF_v1.1.pdf).

# COFF / PE

[COFF](https://en.wikipedia.org/wiki/COFF)
[Wayback Machine: COFF](https://web.archive.org/web/20061216043713/http://support.microsoft.com/?id=121460#)
[Image File Header](https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_file_header)