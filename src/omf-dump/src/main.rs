mod omf {
    use byteorder::{LittleEndian, ReadBytesExt};
    use std::error::Error;
    use std::fs::File;
    use std::io::Read;
    use std::str;

    pub struct ObjectRecord {
        pub record_type: u8,
        pub length: usize,
        pub content: Vec<u8>,
        pub checksum: u8,
    }

    pub fn read_object_records(file_path: &str) -> Result<Vec<ObjectRecord>, Box<dyn Error>> {
        let mut file = File::open(file_path)?;
        let file_len = file.metadata()?.len();

        let mut bytes_read: u64 = 0;
        let mut object_records: Vec<ObjectRecord> = vec![];

        println!("file len: {}", file_len);

        loop {
            let record_type = file.read_u8()?;
            let record_length: usize = file.read_u16::<LittleEndian>()?.into();
            let content_length = record_length - 1;

            bytes_read += 3;

            println!("type: {:#x}, len: {}", record_type, record_length);

            let mut record_content: Vec<u8> = vec![];

            record_content.resize(content_length, 0);
            file.read_exact(&mut record_content)?;
            bytes_read += content_length as u64;

            let checksum = file.read_u8()?;
            bytes_read += 1;

            println!("bytes read: {}", bytes_read);

            object_records.push(ObjectRecord {
                record_type,
                length: content_length,
                content: record_content,
                checksum,
            });

            if bytes_read >= file_len {
                break;
            }
        }

        Ok(object_records)
    }

    // TODO not sure if this is a sound way to calculate the checksum
    fn calc_checksum(object_record: &ObjectRecord) -> u8 {
        // 0x0 means no checksum provided
        // FIXME 0xff seems to be the same, but could not find any information online
        if object_record.checksum == 0x0 || object_record.checksum == 0xff {
            return object_record.checksum;
        }

        let mut sum: i64 = object_record.record_type.into();

        let len = object_record.length.to_le_bytes().map(|b| b as i64);
        sum += len[0] + len[1];

        let content_sum: i64 = object_record.content.iter().map(|b| *b as i64).sum();
        sum += content_sum;

        sum += 1;

        sum = -sum;

        (sum % 256) as u8
    }

    pub fn has_valid_checksum(object_record: &ObjectRecord) -> bool {
        let checksum = calc_checksum(object_record);
        checksum == object_record.checksum
    }

    pub enum CommentClass {
        Translator(String),
        Unsupported(u8),
    }

    pub struct CommentRecord {
        pub no_purge_bit: bool,
        pub no_list_bit: bool,
        pub comment_class: CommentClass,
    }

    pub struct SegmentDefinitionRecord {
        pub alignment: u8,
        pub combination: u8,
        pub big: bool,
        pub use_32: bool,
        pub frame_number: Option<u16>,
        pub offset: Option<u8>,
        pub segment_length: u32,
        pub segment_name_index: u16,
        pub class_name_index: u16,
        pub overlay_name_index: u16,
    }

    pub struct GroupComponent {
        pub type_field: u8,
        pub segment_name_index: u16,
    }

    pub struct GroupDefinitionRecord {
        pub group_name_index: u16,
        pub group_components: Vec<GroupComponent>,
    }

    pub struct PublicName {
        pub public_name_string: String,
        pub public_offset: u32,
        pub type_index: u16,
    }
    pub struct PublicNamesDefinitionRecord {
        pub base_group_index: u16,
        pub base_segment_index: u16,
        pub base_frame: Option<[u8; 2]>,
        pub public_names: Vec<PublicName>,
    }

    pub struct LogicalEnumeratedDataRecord {
        pub segment_index: u16,
        pub enumerated_data_offset: u32,
        pub data_bytes: Vec<u8>,
    }

    pub struct ModuleEndRecord {
        pub main: bool,
        pub start: bool,
        pub segment_bit: bool,
    }

    pub enum ObjectRecordType {
        TranslatorHeader(String),
        Comment(CommentRecord),
        ListOfNames(Vec<String>),
        SegmentDefinition(SegmentDefinitionRecord),
        GroupDefinition(GroupDefinitionRecord),
        PublicNamesDefinition(PublicNamesDefinitionRecord),
        LogicalEnumeratedData(LogicalEnumeratedDataRecord),
        ModuleEnd(ModuleEndRecord),
        Unsupported(u8),
    }

    fn get_string_with_len(content: &[u8], index: usize) -> String {
        let mut end_pos: usize = content[index].into();
        end_pos += 1;

        let beg_pos = index + 1;
        str::from_utf8(&content[beg_pos..end_pos])
            .unwrap()
            .to_string()
    }

    pub fn parse_object_record(object_record: &ObjectRecord, is_32bit: bool) -> ObjectRecordType {
        match object_record.record_type {
            0x80 => {
                let name = get_string_with_len(&object_record.content, 0);
                ObjectRecordType::TranslatorHeader(name.to_string())
            }
            0x88 => {
                let comment_type = object_record.content[0];
                let no_purge_bit = comment_type & 0x80 == 0x80;
                let no_list_bit = comment_type & 0x40 == 0x40;

                // TODO interpret other Comment Classes
                let comment_class = match object_record.content[1] {
                    0x0 => {
                        let translator = str::from_utf8(&object_record.content[2..]).unwrap();
                        CommentClass::Translator(translator.to_string())
                    }
                    c => CommentClass::Unsupported(c),
                };
                ObjectRecordType::Comment(CommentRecord {
                    no_purge_bit,
                    no_list_bit,
                    comment_class,
                })
            }
            0x96 => {
                let mut lnames: Vec<String> = vec![];

                let mut len_read: usize = 0;
                let mut i: usize = 0;
                loop {
                    let str_len: usize = object_record.content[i].into();

                    i += 1;

                    if str_len == 0 {
                        // zero-length string is a valid name
                        lnames.push(String::new());

                        len_read += 1;

                        continue;
                    }

                    let end_pos = i + str_len;
                    let name = str::from_utf8(&object_record.content[i..end_pos]).unwrap();
                    lnames.push(name.to_string());

                    len_read += str_len + 1;
                    i += str_len;

                    if len_read == object_record.length {
                        break;
                    }
                }

                ObjectRecordType::ListOfNames(lnames)
            }
            0x98 | 0x99 => {
                let alignment = object_record.content[0] >> 5;
                let combination = (object_record.content[0] & 0x1C) >> 2;
                let big = (object_record.content[0] & 0x2) == 0x2;
                let use_32 = (object_record.content[0] & 0x1) == 0x1;

                let mut frame_number = None;
                let mut offset = None;
                let mut idx = 1;
                if alignment == 0 {
                    frame_number = Some(u16::from_le_bytes([
                        object_record.content[1],
                        object_record.content[2],
                    ]));
                    offset = Some(object_record.content[3]);

                    idx = 4;
                }

                if object_record.record_type == 0x99 {
                    let segment_length = u32::from_le_bytes([
                        object_record.content[idx],
                        object_record.content[idx + 1],
                        object_record.content[idx + 2],
                        object_record.content[idx + 3],
                    ]);
                    idx += 4;

                    let segment_name_index = u16::from_le_bytes([
                        object_record.content[idx],
                        object_record.content[idx + 1],
                    ]);
                    idx += 2;

                    let class_name_index = u16::from_le_bytes([
                        object_record.content[idx],
                        object_record.content[idx + 1],
                    ]);
                    idx += 2;

                    let overlay_name_index = u16::from_le_bytes([
                        object_record.content[idx],
                        object_record.content[idx + 1],
                    ]);

                    ObjectRecordType::SegmentDefinition(SegmentDefinitionRecord {
                        alignment,
                        combination,
                        big,
                        use_32,
                        frame_number,
                        offset,
                        segment_length,
                        segment_name_index,
                        class_name_index,
                        overlay_name_index,
                    })
                } else {
                    let segment_length: u32 = u16::from_le_bytes([
                        object_record.content[idx],
                        object_record.content[idx + 1],
                    ])
                    .into();
                    idx += 2;

                    let segment_name_index: u16 = object_record.content[idx].into();
                    idx += 1;

                    let class_name_index: u16 = object_record.content[idx].into();
                    idx += 1;

                    let overlay_name_index: u16 = object_record.content[idx].into();

                    ObjectRecordType::SegmentDefinition(SegmentDefinitionRecord {
                        alignment,
                        combination,
                        big,
                        use_32,
                        frame_number,
                        offset,
                        segment_length,
                        segment_name_index,
                        class_name_index,
                        overlay_name_index,
                    })
                }
            }
            0x9a => {
                let group_name_index: u16 = if is_32bit {
                    u16::from_le_bytes([object_record.content[0], object_record.content[1]])
                } else {
                    object_record.content[0].into()
                };

                let mut idx = if is_32bit { 2 } else { 1 };

                let mut group_components: Vec<GroupComponent> = vec![];

                loop {
                    let type_field = object_record.content[idx];
                    let seg_def = if is_32bit {
                        u16::from_le_bytes([
                            object_record.content[idx + 1],
                            object_record.content[idx + 2],
                        ])
                    } else {
                        object_record.content[idx + 1].into()
                    };

                    if is_32bit {
                        idx += 3;
                    } else {
                        idx += 2;
                    }

                    group_components.push(GroupComponent {
                        segment_name_index: seg_def,
                        type_field,
                    });

                    if idx >= object_record.content.len() {
                        break;
                    }
                }

                ObjectRecordType::GroupDefinition(GroupDefinitionRecord {
                    group_name_index,
                    group_components,
                })
            }
            0x90 | 0x91 => {
                if object_record.record_type == 0x91 {
                    let base_group_index =
                        u16::from_le_bytes([object_record.content[0], object_record.content[1]]);
                    let base_segment_index =
                        u16::from_le_bytes([object_record.content[2], object_record.content[3]]);
                    let base_frame = if base_segment_index == 0 {
                        Some([object_record.content[4], object_record.content[5]])
                    } else {
                        None
                    };

                    let mut idx: usize = if base_segment_index == 0 { 6 } else { 4 };
                    let mut public_names: Vec<PublicName> = vec![];
                    loop {
                        let str_len: usize = object_record.content[idx].into();
                        idx += 1;

                        let end_pos = idx + str_len;
                        let public_name_string =
                            str::from_utf8(&object_record.content[idx..end_pos])
                                .unwrap()
                                .to_string();
                        idx += str_len;

                        let public_offset = u32::from_le_bytes([
                            object_record.content[idx],
                            object_record.content[idx + 1],
                            object_record.content[idx + 2],
                            object_record.content[idx + 3],
                        ]);
                        idx += 4;

                        let type_index = u16::from_le_bytes([
                            object_record.content[idx],
                            object_record.content[idx + 1],
                        ]);
                        idx += 2;

                        public_names.push(PublicName {
                            public_name_string,
                            public_offset,
                            type_index,
                        });

                        if idx >= object_record.content.len() {
                            break;
                        }
                    }

                    ObjectRecordType::PublicNamesDefinition(PublicNamesDefinitionRecord {
                        base_group_index,
                        base_segment_index,
                        base_frame,
                        public_names,
                    })
                } else {
                    let base_group_index: u16 = object_record.content[0].into();
                    let base_segment_index: u16 = object_record.content[1].into();
                    let base_frame = if base_segment_index == 0 {
                        Some([object_record.content[2], object_record.content[3]])
                    } else {
                        None
                    };

                    let mut idx: usize = if base_segment_index == 0 { 4 } else { 2 };
                    let mut public_names: Vec<PublicName> = vec![];
                    loop {
                        let str_len: usize = object_record.content[idx].into();
                        idx += 1;

                        let end_pos = idx + str_len;
                        let public_name_string =
                            str::from_utf8(&object_record.content[idx..end_pos])
                                .unwrap()
                                .to_string();
                        idx += str_len;

                        let public_offset: u32 = u16::from_le_bytes([
                            object_record.content[idx],
                            object_record.content[idx + 1],
                        ])
                        .into();
                        idx += 2;

                        let type_index: u16 = object_record.content[idx].into();
                        idx += 1;

                        public_names.push(PublicName {
                            public_name_string,
                            public_offset,
                            type_index,
                        });

                        if idx >= object_record.content.len() {
                            break;
                        }
                    }

                    ObjectRecordType::PublicNamesDefinition(PublicNamesDefinitionRecord {
                        base_group_index,
                        base_segment_index,
                        base_frame,
                        public_names,
                    })
                }
            }
            0xa0 | 0xa1 => {
                if object_record.record_type == 0xa1 {
                    let segment_index =
                        u16::from_le_bytes([object_record.content[0], object_record.content[1]]);
                    let enumerated_data_offset = u32::from_le_bytes([
                        object_record.content[2],
                        object_record.content[3],
                        object_record.content[4],
                        object_record.content[5],
                    ]);
                    let mut data_bytes: Vec<u8> = vec![];
                    data_bytes.extend_from_slice(&object_record.content[6..]);

                    ObjectRecordType::LogicalEnumeratedData(LogicalEnumeratedDataRecord {
                        segment_index,
                        enumerated_data_offset,
                        data_bytes,
                    })
                } else {
                    let segment_index: u16 = object_record.content[0].into();
                    let enumerated_data_offset: u32 =
                        u16::from_le_bytes([object_record.content[1], object_record.content[2]])
                            .into();
                    let mut data_bytes: Vec<u8> = vec![];
                    data_bytes.extend_from_slice(&object_record.content[6..]);

                    ObjectRecordType::LogicalEnumeratedData(LogicalEnumeratedDataRecord {
                        segment_index,
                        enumerated_data_offset,
                        data_bytes,
                    })
                }
            }
            0x8a | 0x8b => {
                let main = object_record.content[0] & 0x80 == 0x80;
                let start = object_record.content[0] & 0x40 == 0x40;
                let segment_bit = object_record.content[0] & 0x20 == 0x20;

                // TODO handling of conditional start address

                ObjectRecordType::ModuleEnd(ModuleEndRecord {
                    main,
                    start,
                    segment_bit,
                })
            }
            t => ObjectRecordType::Unsupported(t),
        }
    }
}

use omf::{
    has_valid_checksum, parse_object_record, read_object_records, CommentClass, ObjectRecordType,
};
use std::io::Write;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let file_name = &args[1];

    // TODO check if this is a valid object file by reading the first byte and making sure that it is a valid record type
    let object_records = read_object_records(file_name).unwrap();
    let is_32bit = object_records.iter().any(|r| r.record_type == 0x99);

    for object_record in &object_records {
        println!(
            "{:#x} ({}), checksum {:#x}",
            object_record.record_type, object_record.length, object_record.checksum
        );
        if !has_valid_checksum(object_record) {
            eprintln!("checksum is invalid!");
            continue;
        }

        match parse_object_record(object_record, is_32bit) {
            ObjectRecordType::TranslatorHeader(name) => {
                println!("80H THEADR - Translator Header Record");
                println!("Name of Object Module: {name}");
                println!();
            }
            ObjectRecordType::Comment(comment_record) => {
                println!("88H COMENT - Comment Record");
                println!(
                    "no_purge_bit: {}, no_list_bit: {}",
                    comment_record.no_purge_bit, comment_record.no_list_bit
                );
                match comment_record.comment_class {
                    CommentClass::Translator(translator) => {
                        println!("Translator: {translator}");
                    }
                    CommentClass::Unsupported(c) => {
                        println!("Unsupported comment class: {:#x}", c);
                    }
                }
                println!();
            }
            ObjectRecordType::ListOfNames(lnames) => {
                println!("96H LNAMES - List of Names Record");
                for (seg_idx, name) in lnames.iter().enumerate() {
                    println!("[{seg_idx}] {name}");
                }
                println!();
            }
            ObjectRecordType::SegmentDefinition(seg_def_record) => {
                println!("98H or 99H SEGDEF - Segment Definition Record");
                println!("Alignment: {}", seg_def_record.alignment);
                println!("Combination: {}", seg_def_record.combination);
                println!("Big: {}", seg_def_record.big);
                println!("Use32: {}", seg_def_record.use_32);
                println!("Frame Number: {:?}", seg_def_record.frame_number);
                println!("Offset: {:?}", seg_def_record.offset);
                println!("Segment Length: {}", seg_def_record.segment_length);
                println!("Segment Name Index: {}", seg_def_record.segment_name_index);
                println!("Class Name Index: {}", seg_def_record.class_name_index);
                println!("Overlay Name Index: {}", seg_def_record.overlay_name_index);
                println!();
            }
            ObjectRecordType::GroupDefinition(grp_def_record) => {
                println!("9AH GRPDEF - Group Definition Record");
                println!("Group Name Index: {}", grp_def_record.group_name_index);

                for grp_def in &grp_def_record.group_components {
                    println!("[{:#x}] {}", grp_def.type_field, grp_def.segment_name_index);
                }

                println!();
            }
            ObjectRecordType::PublicNamesDefinition(pub_names_def_record) => {
                println!("90H or 91H PUBDEF - Public Names Definition Record");
                println!(
                    "Base Group Index: {}",
                    pub_names_def_record.base_group_index
                );
                println!(
                    "Base Segment Index: {}",
                    pub_names_def_record.base_segment_index
                );
                println!("Base Frame: {:?}", pub_names_def_record.base_frame);
                for pub_name in &pub_names_def_record.public_names {
                    println!(
                        "{}, Public Offset: {}, Type Index: {}",
                        pub_name.public_name_string, pub_name.public_offset, pub_name.type_index
                    );
                }
                println!();
            }
            ObjectRecordType::LogicalEnumeratedData(logical_enumerated_data_record) => {
                println!("A0H or A1H LEDATA - Logical Enumerated Data Record");
                println!(
                    "Segment Index: {}",
                    logical_enumerated_data_record.segment_index
                );
                println!(
                    "Enumerated Data Offset: {}",
                    logical_enumerated_data_record.enumerated_data_offset
                );
                println!("{:?}", logical_enumerated_data_record.data_bytes);
                println!();
            }
            ObjectRecordType::ModuleEnd(module_end_record) => {
                println!("8AH or 8BH MODEND - Module End Record");
                println!("Main: {}", module_end_record.main);
                println!("Start: {}", module_end_record.start);
                println!("Segment Bit: {}", module_end_record.segment_bit);
                println!();
            }
            ObjectRecordType::Unsupported(t) => {
                println!("Unsupported object record type: {:#x}", t);
                println!();
            }
        }
    }

    let mut idx = 0;
    for object_record in object_records.iter().filter(|r| r.record_type == 0xa0) {
        match parse_object_record(object_record, is_32bit) {
            ObjectRecordType::LogicalEnumeratedData(logical_enumerated_data_record) => {
                let out_file_name = format!("ledata_{}.bin", idx);
                println!(
                    "Writing {} bytes to {}",
                    logical_enumerated_data_record.data_bytes.len(),
                    &out_file_name
                );
                let mut out_file = std::fs::File::create(&out_file_name).unwrap();
                out_file
                    .write_all(&logical_enumerated_data_record.data_bytes)
                    .unwrap();

                idx += 1;
            }
            _ => (),
        }
    }
}
