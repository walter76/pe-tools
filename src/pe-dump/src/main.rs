use std::{env, ptr::read};
use std::path::Path;
use std::fs::File;
use byteorder::{ReadBytesExt, LittleEndian};
use std::io::Read;

// default is LittleEndian byte order

struct DOSHeader {
    e_magic: u16,       // Magic number
    e_cblp: u16,        // Bytes on last page of file
    e_cp: u16,          // Pages in file
    e_crlc: u16,        // Relocations
    e_cparhdr: u16,     // Size of header in paragraphs
    e_minalloc: u16,    // Minimum extra paragraphs needed
    e_maxalloc: u16,    // Maximum extra paragraphs needed
    e_ss: u16,          // Initial (relative) SS (Stack Segment) value
    e_sp: u16,          // Initial SP (Stack Pointer) value
    e_csum: u16,        // Checksum
    e_ip: u16,          // Initial IP (Instruction Pointer) value
    e_cs: u16,          // Initial (relative) CS (Code Segment) value
    e_lfarlc: u16,      // File address of relocation table
    e_ovno: u16,        // Overlay number
    e_res: [u16; 4],    // Reserved words
    e_oemid: u16,       // OEM identifier (for e_oeminfo)
    e_oeminfo: u16,     // OEM information; e_oemid specific
    e_res2: [u16; 10],  // Reserved words
    e_lfanew: u32,      // File address of new exe header
}

fn read_dos_header(file: &mut File) -> DOSHeader {
    let e_magic = file.read_u16::<LittleEndian>().unwrap();
    let e_cblp = file.read_u16::<LittleEndian>().unwrap();
    let e_cp = file.read_u16::<LittleEndian>().unwrap();
    let e_crlc = file.read_u16::<LittleEndian>().unwrap();
    let e_cparhdr = file.read_u16::<LittleEndian>().unwrap();
    let e_minalloc = file.read_u16::<LittleEndian>().unwrap();
    let e_maxalloc = file.read_u16::<LittleEndian>().unwrap();
    let e_ss = file.read_u16::<LittleEndian>().unwrap();
    let e_sp = file.read_u16::<LittleEndian>().unwrap();
    let e_csum = file.read_u16::<LittleEndian>().unwrap();
    let e_ip = file.read_u16::<LittleEndian>().unwrap();
    let e_cs = file.read_u16::<LittleEndian>().unwrap();
    let e_lfarlc = file.read_u16::<LittleEndian>().unwrap();
    let e_ovno = file.read_u16::<LittleEndian>().unwrap();

    let mut e_res: [u16; 4] = [0; 4];
    for w in e_res.iter_mut() {
        *w = file.read_u16::<LittleEndian>().unwrap();
    }

    let e_oemid = file.read_u16::<LittleEndian>().unwrap();
    let e_oeminfo = file.read_u16::<LittleEndian>().unwrap();

    let mut e_res2: [u16; 10] = [0; 10];
    for w in e_res2.iter_mut() {
        *w = file.read_u16::<LittleEndian>().unwrap();
    }

    let e_lfanew = file.read_u32::<LittleEndian>().unwrap();

    DOSHeader {
        e_magic,
        e_cblp,
        e_cp,
        e_crlc,
        e_cparhdr,
        e_minalloc,
        e_maxalloc,
        e_ss,
        e_sp,
        e_csum,
        e_ip,
        e_cs,
        e_lfarlc,
        e_ovno,
        e_res,
        e_oemid,
        e_oeminfo,
        e_res2,
        e_lfanew
    }
}

fn main() {
    let args: Vec<String> =env::args().collect();
    let file_path = Path::new(&args[1]);
    let mut file = File::open(file_path).expect("cannot open file");
    let dos_header = read_dos_header(&mut file);

    println!("0:  Magic number:                             {:#x}", dos_header.e_magic);
    println!("2:  Bytes on last page of file:               {:#x}", dos_header.e_cblp);
    println!("4:  Pages in file:                            {:#x}", dos_header.e_cp);
    println!("6:  Relocations:                              {:#x}", dos_header.e_crlc);
    println!("8:  Size of header in paragraphs:             {:#x}", dos_header.e_cparhdr);
    println!("A:  Minimum extra paragraphs needed:          {:#x}", dos_header.e_minalloc);
    println!("C:  Maximum extra paragraphs needed:          {:#x}", dos_header.e_maxalloc);
    println!("E:  Initial (relative) SS value:              {:#x}", dos_header.e_ss);
    println!("10: Initial SP value:                         {:#x}", dos_header.e_sp);
    println!("12: Checksum:                                 {:#x}", dos_header.e_csum);
    println!("14: Initial IP value:                         {:#x}", dos_header.e_ip);
    println!("16: Initial (relative) CS value:              {:#x}", dos_header.e_cs);
    println!("18: File address of relocation table:         {:#x}", dos_header.e_lfarlc);
    println!("1A: Overlay number:                           {:#x}", dos_header.e_ovno);
    println!("1C: Reserved words [4]:                            ");
    println!("24: OEM identifier (for OEM information):     {:#x}", dos_header.e_oemid);
    println!("26: OEM information; OEM identifier specific: {:#x}", dos_header.e_oeminfo);
    println!("28: Reserved words [10]:                           ");
    println!("3C: File address of new exe header:           {:#x}", dos_header.e_lfanew);
}
